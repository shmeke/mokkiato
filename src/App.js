import React from "react"
import Header from "./elements/header"
import Start from "./elements/start"
import AboutUs from "./elements/aboutus"
import Offers from "./elements/whatweoffer_New"
import Contact from "./elements/contact"
import Footer from "./elements/footer"

const App = () => {
  return (
    <>
      <Header />
      <Start id="start" /> 
     {/* <AboutUs id="aboutus" /> */}
      <Offers id="whatweoffer" />
      <Contact id="contact" />
      <Footer />
    </>
  )
}

export default App
