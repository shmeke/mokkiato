import React from "react"
import "../styles/aboutus.css"
import "../styles/wrapper.css"
import pic from "../images/profile.png"

const aboutus = () => {
  return (
    <div id="aboutus" className="external-container">
      <div className="wrapper">
        <div className="container-a">
          <div className="header-box">
            <h3>Vi på Mokkiat bygger proffetionella websidor för att passa dina behov samt får dig att synas genom effektiv marknadsföring.</h3>
          </div>

          <div className="container-a-p">
            <div className="person-container">
              <img src={pic} alt="profPic" />
              <div className="person-wrapper">
                <h4>Sebastian Göransson</h4>
                <p>Developer</p>
              </div>
              <div className="contact-info">
                <h6>sebastian.goransson@mokkiato.com</h6>
                <h6>0706345468</h6>
              </div>
            </div>
            <div className="person-container">
              <img src={pic} alt="profPic" />
              <div className="person-wrapper">
                <h4>Fredrik Zandén</h4>
                <p>Marketing</p>
              </div>
              <div className="contact-info">
                <h6>fredrik.zanden@mokkiato.com</h6>
                <h6>0706345468</h6>
              </div>
            </div>
            <div className="person-container">
              <img src={pic} alt="profPic" />
              <div className="person-wrapper">
                <h4>Daniel Göransson</h4>
                <p>Social Media</p>
              </div>
              <div className="contact-info">
                <h6>daniel.goransson@mokkiato.com</h6>
                <h6>0706345468</h6>
              </div>
            </div>
          </div>
        </div>
      </div>  
    </div>
  )
}

export default aboutus
