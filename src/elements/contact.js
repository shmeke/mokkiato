import React, { useState, useRef, useEffect } from "react"
import axios from "axios"
import "../styles/contactus.css"
import "../styles/wrapper.css"
import emailJs from "emailjs-com"

const Contactus = () => {
  const [checked, setChecked] = useState([
    {
      id: 1,
      Selected: false,
      Name: "Basic"
    },
    {
      id: 2,
      Selected: false,
      Name: "Standard"
    },
    {
      id: 3,
      Selected: false,
      Name: "Premium"
    }
  ])
  const [showMessage, setShowMessage] = useState(false)
  const [showMessageErr, setShowMessageErr] = useState(false)
  const [formData, setFormData] = useState({
    email: "",
    message: "",
    type: ""
  })

  const checkList = ["Basic", "Sandard", "Premium"]
  const serviceID = "service_v853ppz"
  const template = "template_gpz4p4t"
  const APIKey = "w7YyQQD2TYtrnQkQZ"

  function sendRequest(e) {
    e.preventDefault()

    if (!formData.email | !formData.message | !formData.type) {
      setShowMessageErr(true)
      setShowMessage(false)
    } else {
      console.log(formData)
      emailJs.init(APIKey)
      emailJs
        .send(serviceID, template, {
          to_name: "Sebastian",
          message: formData.message,
          from_email: formData.email,
          website_type: formData.type
        })
        .then(
          result => {
            console.log(result.text)
            setShowMessage(true)
            setShowMessageErr(false)
            setFormData({ email: "", message: "", type: "" })
            setChecked([
              { id: 1, Selected: false, Name: "Basic" },
              { id: 2, Selected: false, Name: "Standard" },
              { id: 3, Selected: false, Name: "Premium" }
            ])
          },
          error => {
            console.log(error)
          }
        )
      emailJs
        .send("service_v853ppz", "template_z6zcudc", {
          to_email: formData.email,
          website_type: formData.Type
        })
        .then(
          result => {
            console.log(result.text)
          },
          error => {
            console.log(error)
          }
        )
    }
  }

  /* const handleCheck = event => {
    var updatedList = [...checked]
    if (event.target.checked) {
      updatedList = [...checked, event.target.value]
      setFormData({ ...formData, type: updatedList })
    } else {
      updatedList.splice(checked.indexOf(event.target.value), 1)
      setFormData({ ...formData, type: updatedList })
    }
    setChecked(updatedList)
  }*/

  function handleCheck(id) {
    let newItems = []
    checked.forEach(item => {
      if (item.id === id) {
        newItems.push({ id: id, Selected: true, Name: item.Name })
        setFormData({ ...formData, type: item.Name })
      } else {
        newItems.push({ id: item.id, Selected: false, Name: item.Name })
      }
    })
    setChecked(newItems)
  }

  useEffect(() => {
    console.log(checked)
    console.log(formData)
  }, [checked])

  return (
    <div id="contact" className="external-container-c">
      <div className="wrapper-form">
        <div className="container-c">
          <div className="container-c-info">
            <h3>Kontakt</h3>
            <p>Skicka in en förfrågan via nedan formulär så kontaktar vi dig så snart vi kan!</p>
          </div>

          <div>
            {showMessage ? <div className="alert-box success">Förfrågan skickad!</div> : null}
            {showMessageErr ? <div className="alert-box error">Vänligen fyll i alla fält i formuläret</div> : null}
            <form className="container-form" onSubmit={sendRequest}>
              <input
                onChange={e => {
                  setFormData({ ...formData, email: e.target.value })
                }}
                value={formData.email}
                type="email"
                id="email"
                placeholder="Email"
                pattern="[A-Za-z0-9]{3}"
              ></input>
              <textarea
                onChange={e => {
                  setFormData({ ...formData, message: e.target.value })
                }}
                value={formData.message}
                type="text"
                id="message"
                placeholder="Skriv ett meddelande..."
                pattern="[A-Za-z0-9]{3}"
              ></textarea>
              <p>Markera paketet du vil ha:</p>
              <div className="checkbox-container">
                {checked.map((item, index) => (
                  <div key={index}>
                    <input value={item.Selected} checked={item.Selected} type="checkbox" onChange={e => handleCheck(item.id)} />
                    <span>{item.Name}</span>
                  </div>
                ))}
              </div>
              <input type="submit" value="Skicka" />
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Contactus
