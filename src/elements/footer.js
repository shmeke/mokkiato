import React from "react"
import "../styles/footer.css"
import fb from "../icons/facebook.png"
import ins from "../icons/instagram.png"
import lin from "../icons/linkedin.png"
import "../styles/wrapper.css"

const footer = () => {
  return (
    <div className="social-container">
      <div className="flex-container">
        <div className="social-icons">
          <a href="https://www.facebook.com/" className="fb soical">
            <img src={fb} alt="facebook" />
          </a>
        </div>
        <div className="social-icons">
          <a href="https://www.instagram.com/" className="ins soical">
            <img src={ins} alt="instagram" />
          </a>
        </div>

        <div className="social-icons">
          <a href="https://www.linkedin.com/" className="ln soical">
            <img src={lin} alt="linkedIn" />
          </a>
        </div>
      </div>
    </div>
  )
}

export default footer
