import React from "react"
import { Link } from "react-scroll"
import "../styles/header.css"

const header = () => {
  return (
    <div className="header-container">
      <ul className="header">
        <li>
          <Link activeClass="active" to="start" spy={true} smooth={true}>
            Start
          </Link>
        </li>
        {/*<li>
          <Link to="aboutus" spy={true} smooth={true}>
            Om oss
          </Link>
  </li> */}
        <li>
          <Link to="whatweoffer" spy={true} smooth={true}>
            Vad vi erbjuder
          </Link>
        </li>
        <li>
          <Link to="contact" spy={true} smooth={true}>
            Kontakt
          </Link>
        </li>
      </ul>
    </div>
  )
}

export default header
