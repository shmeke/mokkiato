import React from "react"
import "../styles/start.css"

const start = () => {
  return (
    <div id="start" className="container">
      <div className="backdrop">
        {/* <img src={backD} /> */}
        <div className="backdrop-slogan">
          <h3>Mokkiato Webdesign och Marknadsföring</h3>
          <p>Hemsidor, Webdesign & Marknadsföring</p>
        </div>
      </div>
    </div>
  )
}

export default start
