import React, { useState } from "react"
import "../styles/whatweoffer.css"
import "../styles/wrapper.css"
import markt from "../images/markt.jpg"
import sMed from "../images/sMedia.jpg"
import webD from "../images/webDes.jpg"
import check from "../icons/check3.png"
import checkG from "../icons/reyx3.png"

export default function Whatweoffer() {

  return (
    <div id="whatweoffer" className="external-container-b">
      <div className="wrapper">
        <div className="container-b">
          <h3>Vad vi erbjuder</h3>
          <div className="master-box">
            <div className="box">
              <div className="box-text">
                {/*<div className="box-img">
                  <img src={webD} alt="" />
  </div>*/}
                <h4>Basic</h4>
                <p> Från 560:-</p>
                <div className="included_in_package"> 
                <div className="package-description">
                <label>
                    <img src={check} />
                    Design anpassning
                  </label>
                </div>
                <div className="package-description">
                  <label>
                      <img src={checkG} />
                      Inkluderar källkod
                  </label>
                </div>

                </div>     
              </div>
            </div>

            <div className="box">
              <div className="box-text">
                <div className="box-img">
                  <img src={markt} alt="" />
                </div>
                <h4>Standard</h4>
                <p>Från 1450:-</p>
              </div>
            </div>

            <div className="box">
              <div className="box-text">
                <div className="box-img">
                  <img src={sMed} alt="" />
                </div>
                <h4>Premium</h4>
                <p>Från 4440:-</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
