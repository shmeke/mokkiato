import react, { useState } from "react";
import "../styles/whatweoffer.css"
import "../styles/wrapper.css"
import check from "../icons/check3.png"
import checkG from "../icons/xIcon.png"

const whatweoffer_New = () => {
    const packages = [
        "Basic",
        "Standard",
        "Premium"
    ]
        const arr = [  
            {"type":"Responsiv Design","basic":check,"standard":check,"premium":check, "typeOF":"img"},
            {"type":"Designanpassning","basic":check,"standard":check,"premium":check, "typeOF":"img"},
            {"type":"Källkod inkluderad","basic":checkG,"standard":check,"premium":check, "typeOF":"img"},
            {"type":"Innehålls uppladdning","basic":checkG,"standard":checkG,"premium":check, "typeOF":"img"},
            {"type":"Antal sidor","basic":3,"standard":5,"premium":10, "typeOF":"int"},
            {"type":"Licensierade bilder","basic":1,"standard":5,"premium":10, "typeOF":"int"},
            {"type":"Revisioner","basic":1,"standard":5,"premium":"unlimited", "typeOF":"int"},
            {"type":"Leveranstid","basic":"6 dagar","standard":"4 dagar","premium":"2 dagar", "typeOF":"int"},
            {"type":"Pris","basic":"580:-","standard":"1700:-","premium":"4800:-", "typeOF":"int"},
        ]

    console.log(arr)
    return (
        <div id="whatweoffer" className="external-container-b">
            <div className="wrapper">
                <div className="container-b">
                    <h3>Vad vi erbjuder</h3>    
                

                <div className="container-offers">
                    <table>
                        <thead>
                            <th>Paket</th>
                            <th>Basic</th>
                            <th>Standard</th>
                            <th>Premium</th>
                        </thead>
                        <tbody className="bod">
                            {arr.map(pack => {
                                if(pack.typeOF === "img") {
                                    return(
                                        <tr key={pack.type}>
                                            <td>{pack.type}</td>
                                            <td><img src={pack.basic} /></td>
                                            <td><img src={pack.standard} /></td>
                                            <td><img src={pack.premium} /></td>
                                        </tr>
                                    ) 
                                } else {
                                    return (
                                        <tr key={pack.type}>
                                            <td>{pack.type}</td>
                                            <td>{pack.basic}</td>
                                            <td>{pack.standard}</td>
                                            <td>{pack.premium}</td>
                                        </tr>
                                    )
                                }                            
                            })} 
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>

    )
}

export default whatweoffer_New